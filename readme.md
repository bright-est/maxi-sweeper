# MineField API

## Constructor

    var minefield = new minesweeper.Minefield(width, height, minesCount)

Arguments

* width [int]
* height [int]
* minesCount [int]

## Properties

* width [int] (readonly) - width of the minefield.
* height [int] (readonly) - height of the minefield.
* state [int] (readonly) - state of the minefield.

## States

All possible states are defined in minesweeper.FieldState.
Which are:

* NOT_ACTIVE
* ACTIVE
* EXPLODED
* DEMINED


## Functions

### reset()

Resets the state of the minefield back to NOT_ACTIVE.
So the next stepOn function will mine new areas.

### stepOn(x, y)

Function to discover the area on the minefield.

First step is always safe area.
This is when mines are place on the minefield and state is set to ACTIVE.

Result is an array of all safe areas around the area which was stepped on (including itself).

    [
        {
            coordinates: {x: x, y: y},
            minesNear: minesNear
        },
        ...
    ]

## Events

### stateChange(handler)

Can register callback handler for state change event.

    minefield.stateChange(function (state, data) {
        // only state EXPLODED have data, which is list of all mined areas.
    });

data structure of mined areas list

    {
        "x;y": {x: x, y: y},
        ...
    }


### stepped(handler)

Registeres handler for stepOn event.

	minefield.stepped(function (x, y, safeAreas) {
		//
	});
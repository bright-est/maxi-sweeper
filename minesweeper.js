(function() {

    var state = {
        NOT_ACTIVE: 1,
        ACTIVE: 2,
        EXPLODED: 3,
        DEMINED: 4,
    };

	var Coordinates = (function () {

		function Coordinates(x, y) {

			var _this = this;

			this.x = x;
			this.y = y;

			this.getKey = function() {
				return getKey(x, y);
			};

			this.add = function(addX, addY) {
				return new Coordinates(_this.x + addX, _this.y + addY);
			};
		}

		Coordinates.getKey = getKey;

		function getKey(x, y) {
			return x + ";" + y;
		}

		return Coordinates;

	})();



    function Minefield(width, height, minesCount) {

        var _this = this;
        var _width = width;
        var _height = height;
        var _minesCount = minesCount;

        var _minedAreas = {};
        var _deminedAreas = {};
        var _state = state.NOT_ACTIVE;

        sparkling.Observable.mixin(this);

        Object.defineProperties(this, {
            "width": {
                get: function () { return _width; }
            },
            "height": {
                get: function () { return _height; }
            },
            "state": {
                get: function () { return _state; }
            }
        });

        this.reset = function () {
            _minedAreas = {};
            _deminedAreas = {};
            setState(state.NOT_ACTIVE);
        };

        this.stepOn = function (x, y) {

            var res = stepOn(x, y);
			_this.publish("stepOn", x, y, res);
			return res;
        };

        this.stateChange = function (handler) {
            _this.subscribe("stateChange", handler);
        };

		this.stepped = function (handler) {
			_this.subscribe("stepOn", handler);
		};

		function stepOn(x, y) {
			var coordinates = new Coordinates(x, y);

            if (_state === state.NOT_ACTIVE) {
                setMinedAreas(coordinates);
                setState(state.ACTIVE);
            }

            if (_state === state.ACTIVE && isMined(coordinates)) {
                setState(state.EXPLODED, _minedAreas);
            }

            if (_state === state.ACTIVE) {
                var newSafeAreas = getNearSafeAreas(coordinates);

                if (isAllDemined(newSafeAreas)){
                    setState(state.DEMINED);
                }

                return newSafeAreas;
            }

            return [];
		}

        function isMined(coordinates) {
            return _minedAreas.hasOwnProperty(coordinates.getKey());
        }

        function setState(state, data) {
            if (_state === state) {
                return;
            }

            _state = state;
            _this.publish("stateChange", state, data);
        }

        function isAllDemined(newSafeAreas) {
            for (var i = 0; i < newSafeAreas.length; i++) {
                _deminedAreas[newSafeAreas[i].coordinates.getKey()] = true;
            }

            return Object.keys(_deminedAreas).length === _width * _height - _minesCount;
        }

        function setMinedAreas(excludedArea) {

            // use list of all yet unmined areas to ensure unique mined coorinates generation
            var array = createAreasList(excludedArea);

            for (var i = 0; i < _minesCount; i++) {
                var rnd = Math.floor(Math.random() * array.length);

                var coordinates = array[rnd];
                _minedAreas[coordinates.getKey()] = coordinates;

                // remove mined area from unmined areas list
                array.splice(rnd, 1);
            }
        }

        function createAreasList(excludedArea) {
            var areasList = [];
            for (var x = 0; x < _width; x++) {
                for (var y = 0; y < _height; y++) {
                    if (x === excludedArea.x && y === excludedArea.y) {
                        continue;
                    }
                    areasList.push(new Coordinates(x, y));
                }
            }
            return areasList;
        }

        function getNearSafeAreas(coordinates) {

            var minesCountNearBy = getMinesCountNearBy(coordinates);

			var res = [{
                coordinates: coordinates,
                minesNear: minesCountNearBy
            }];

            if (minesCountNearBy === 0) {
	            expand(res, coordinates);
            }

			return res;
        }

		function expand(res, coordinates) {

			var checklist = {};
			var prevEmptyAreas = [ coordinates ];
			while (prevEmptyAreas.length > 0)
			{
				var emptyAreas = [];

				for (var i = 0; i < prevEmptyAreas.length; i++) {

					var c = prevEmptyAreas[i];

					for (var x = -1; x <= 1; x++) {

						var newX = c.x + x;
						if (newX < 0 || newX > _width-1) {
							continue;
						}

						for (var y = -1; y <= 1; y++) {
							if (x === 0 && y === 0) {
								continue;
							}

							var newY = c.y + y;
							if (newY  < 0 || newY > _height-1) {
								continue;
							}

							var neighbourKey = Coordinates.getKey(newX, newY);
							if (checklist.hasOwnProperty(neighbourKey)) {
								continue;
							}

							checklist[neighbourKey] = 1;

							var neighbour = new Coordinates(newX, newY);
							var minesNear = getMinesCountNearBy(neighbour);

							res.push({
								coordinates: neighbour,
								minesNear: minesNear
							});

							if (minesNear === 0) {
								emptyAreas.push(neighbour);
							}
						}
					}
				}

				prevEmptyAreas = emptyAreas;
			}
		}

        function getMinesCountNearBy(coordinates) {
            var minesNear = 0;

			for (var x = -1; x <= 1; x++) {
				for (var y = -1; y <= 1; y++) {
					if (x === 0 && y === 0) {
						continue;
					}
					if (isMined(coordinates.add(x, y))) {
						minesNear++;
					}
				}
			}

            return minesNear;
        }
    }

    window.minesweeper = {
        Minefield: Minefield,
        FieldState: state
    };

})();
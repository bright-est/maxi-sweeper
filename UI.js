minesweeper.MinefieldUI = function MinefieldUI(minefield) {

    var _elContainer = document.getElementById("container");
    var _elBtnReset = document.getElementById("btn-reset");

    var _cells = [];
    createField();

    minefield.stateChange(function (state, data) {

        switch (state) {
            case minesweeper.FieldState.NOT_ACTIVE:
                _elContainer.className = "";
                break;
            case minesweeper.FieldState.ACTIVE:
                _elContainer.className = "active";
                break;
            case minesweeper.FieldState.EXPLODED:
                _elContainer.className = "exploded";

                for (var i in data) {
                    if (data.hasOwnProperty(i)) {
                        _cells[data[i].x][data[i].y].classList.add("mine");
                    }
                }

                alert("BOOM! You're dead!'");

                break;
            case minesweeper.FieldState.DEMINED:
                _elContainer.className = "demined";

                alert("YEAH! All clear!");

                break;
        }

    });

	minefield.stepped(function (x, y, safeAreas) {
		openSafeAreas(safeAreas);
	});

    _elBtnReset.addEventListener("click", function (e) {
        e.preventDefault();

        createField();
        minefield.reset();
    });

    function createField() {

        _cells = [];

        while (_elContainer.firstChild) {
            _elContainer.removeChild(_elContainer.firstChild);
        }

        for (var y = 0; y < minefield.height; y++) {

            var row = document.createElement("div");
            row.className = "row";
            _elContainer.appendChild(row);

            for (var x = 0; x < minefield.width; x++) {

                if (!_cells[x]) {
                    _cells[x] = [];
                }

                var cell = document.createElement("div");
                cell.className = "cell";
                row.appendChild(cell);
                bindCell(cell, x, y);

                _cells[x][y] = cell;
            }
        }
    }

    function bindCell(cell, x, y) {

        cell.addEventListener('contextmenu', e => e.preventDefault());
        cell.addEventListener("mousedown", function (e) {

            e.preventDefault();

            if (e.which === 3) {
                toggleFlag(this);
                return;
            }

            if (this.classList.contains("flagged")) {
                return;
            }

			console.time("get safe areas");
            minefield.stepOn(x, y);
			console.timeEnd("get safe areas");
        });
    }

	function toggleFlag(cell) {
		if (cell.classList.contains("flagged")) {
			cell.classList.remove("flagged");
		}
		else {
			cell.classList.add("flagged");
		}
	}

	function openSafeAreas(safeAreas) {
		for (var i = 0; i < safeAreas.length; i++) {
			var safeArea = safeAreas[i];
			var cell = _cells[safeArea.coordinates.x][safeArea.coordinates.y];
			var minesNear = safeArea.minesNear;
			if (minesNear === 0) {
				cell.classList.add("clear");
			}
			else {
				cell.classList.add("has-mines-near");
				cell.innerHTML = minesNear;
			}
		}
	}
};